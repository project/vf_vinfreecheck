README.txt

=======================================
INSTALLATION
=======================================

* Download and install the Unirest for PHP library from http://unirest.io/php into sites/all/libraries/unirest-php
* Download and install the Libraries API module from https://www.drupal.org/project/libraries
* Get a Mashape API key from https://www.mashape.com/vinfreecheck/vin-decoder
* Enable the vf_vinfreecheck module
* Configure the key at admin/config/content/vinfield/vf_vinfreecheck